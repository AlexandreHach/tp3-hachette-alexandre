package com.example.mainactivity;

import android.util.JsonReader;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class JSONResponseHandlerRanking {
    private static final String TAG = JSONResponseHandlerTeam.class.getSimpleName();


    private Team team;


    public JSONResponseHandlerRanking(Team team) {
        this.team = team;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readTeams(reader);
        } finally {
            reader.close();
        }
    }

    public void readTeams(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("table")) {
                readArrayTeams(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }


    private void readArrayTeams(JsonReader reader) throws IOException {
        reader.beginArray();
        int classement = 1 ;
        int i =0;
        while (reader.hasNext() ) {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if(name.equals("name")) {
                    String nomTeam = reader.nextString();
                    if (nomTeam.equals(team.getName())) {
                        while (reader.hasNext()) {
                            name = reader.nextName();
                            if (name.equals("total")) {
                                team.setTotalPoints(reader.nextInt());
                                team.setRanking(classement);
                            } else {
                                reader.skipValue();
                            }
                        }
                    }
                }
                else{
                    reader.skipValue();

                }
            }
            reader.endObject();
            classement++;
            i++;
        }
        reader.endArray();
    }
}
