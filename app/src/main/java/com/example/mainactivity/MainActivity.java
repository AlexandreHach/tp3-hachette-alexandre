package com.example.mainactivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import java.net.URL;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {
    SportDbHelper dbHelper = new SportDbHelper(this);
    SwipeRefreshLayout swipeRefreshLayout;
    List<Team> listTeam;
    Team team;
    SimpleCursorAdapter simpleCursor;
    boolean running = false;
    private static final String TAG = TeamActivity.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        swipeRefreshLayout  =  findViewById(R.id.swipe_container);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            Updater updater = new Updater();

            @Override
            public void onRefresh() {
                listTeam = dbHelper.getAllTeams();
                String str_result="";
                AsyncTaskRunner runner = new AsyncTaskRunner();
                runner.execute(listTeam);

                swipeRefreshLayout.setRefreshing(false);

            }
        });

        dbHelper.resetTable();
        setListView();


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, NewTeamActivity.class);
                MainActivity.this.startActivityForResult(myIntent,1);
            }
        });
    }

    private class AsyncTaskRunner extends AsyncTask<List<Team>, String, String> {

        private String resp;

        @Override
        protected String doInBackground(List<Team>... params) {
            Updater updater = new Updater();
            for(int i =0; i < params[0].size(); i ++)
            {
                team = params[0].get(i);
                updater.update(team);
                Log.d(TAG, String.valueOf(team));
                publishProgress();
                SystemClock.sleep(20);// Pour ne pas que le for execute un thread qui n'a pas encorre fais l'update
            }

            return "finish";
        }

        @Override
        protected void onProgressUpdate(String... result) {
            Log.d("onprogress", String.valueOf(team));
            dbHelper.updateTeam(team);
            simpleCursor.changeCursor(dbHelper.fetchAllTeams());
            simpleCursor.notifyDataSetChanged();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: ");
        if (requestCode == 2) {

            if (resultCode == 2) {
                Team team = data.getExtras().getParcelable("team");
                Log.d("intent", String.valueOf(team));
                dbHelper.updateTeam(team);
                setListView();
            }
        }
    }

    protected void setListView() {
        Cursor cursor = dbHelper.fetchAllTeams();
        simpleCursor = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                cursor,
                new String[] { SportDbHelper.COLUMN_TEAM_NAME, SportDbHelper.COLUMN_LEAGUE_NAME },
                new int[] { android.R.id.text1, android.R.id.text2});


        ListView lv = (ListView) findViewById(R.id.listview);
        lv.setAdapter(simpleCursor);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent myIntent = new Intent(MainActivity.this, TeamActivity.class);
                final Cursor item = (Cursor) parent.getItemAtPosition(position);
                Team team = (Team) SportDbHelper.cursorToTeam(item);
                myIntent.putExtra("Team",team);
                MainActivity.this.startActivityForResult(myIntent,2);
            }
        });
        lv.setOnCreateContextMenuListener(this);
    }






}
