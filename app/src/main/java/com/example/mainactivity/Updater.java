package com.example.mainactivity;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Updater {

    private URL urlTeam;
    private WebServiceUrl webServiceUrl = new WebServiceUrl();
    private Team team;

    public void update(Team team) {
        HttpURLConnection urlConnection = null;
        try {
            urlTeam = webServiceUrl.buildSearchTeam(team.getName());
            urlConnection = (java.net.HttpURLConnection) urlTeam.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            JSONResponseHandlerTeam jsonResponseHandlerTeam = new JSONResponseHandlerTeam(team);
            jsonResponseHandlerTeam.readJsonStream(in);
        } catch (
                MalformedURLException e) {
            e.printStackTrace();
        } catch (
                IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }
        try {
            urlTeam = webServiceUrl.buildSearchLastEvents(team.getIdTeam());
            Log.i("urlTeam", String.valueOf(urlTeam));
            urlConnection = (HttpURLConnection) urlTeam.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            JSONResponseHandlerLastEvent jsonResponseHandlerLastEvent = new JSONResponseHandlerLastEvent(team);
            jsonResponseHandlerLastEvent.readJsonStream(in);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }
        try {
            urlTeam = webServiceUrl.buildGetRanking(team.getIdLeague());
            urlConnection = (HttpURLConnection) urlTeam.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            JSONResponseHandlerRanking jsonResponseHandlerRanking = new JSONResponseHandlerRanking(team);
            jsonResponseHandlerRanking.readJsonStream(in);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }
    }

}
