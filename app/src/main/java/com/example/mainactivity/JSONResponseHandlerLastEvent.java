package com.example.mainactivity;

import android.util.JsonReader;
import android.util.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class JSONResponseHandlerLastEvent {
    private static final String TAG = JSONResponseHandlerTeam.class.getSimpleName();


    private Team team;


    public JSONResponseHandlerLastEvent(Team team) {
        this.team = team;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readTeams(reader);
        } finally {
            reader.close();
        }
    }

    public void readTeams(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("results")) {
                readArrayTeams(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }

    private void readArrayTeams(JsonReader reader) throws IOException {
        reader.beginArray();
        Match lastEvent = new Match();
        int intRound = 0;
        int scoreHome = 0;
        int scoreAway = 0;
        boolean saisie = false;
        while (reader.hasNext() ) {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if(saisie == false) {
                    if (name.equals("intHomeScore")) {
                        if (reader.peek().equals(JsonToken.NULL)) {
                            lastEvent.setHomeScore(0);
                            reader.skipValue();
                        } else {
                            scoreHome = reader.nextInt();
                            lastEvent.setHomeScore(scoreHome);
                        }
                    } else if (name.equals("intAwayScore")) {
                        if (reader.peek().equals(JsonToken.NULL)) {
                            lastEvent.setHomeScore(0);
                            reader.skipValue();
                        } else {
                            scoreAway = reader.nextInt();
                            lastEvent.setAwayScore(scoreAway);
                        }
                    } else if (name.equals("strHomeTeam")) {
                        lastEvent.setHomeTeam(reader.nextString());
                    } else if (name.equals("strAwayTeam")) {
                        lastEvent.setAwayTeam(reader.nextString());
                    } else if (name.equals("strEvent")) {
                        lastEvent.setLabel(reader.nextString());
                    } else {
                        reader.skipValue();
                    }
                    if (scoreAway != 0 && scoreHome !=0) {
                        saisie = true;
                    }
                    if (saisie) {
                        team.setLastEvent(lastEvent);
                    }
                }
                else{
                    reader.skipValue();
                }

            }
            reader.endObject();
        }
        reader.endArray();
    }
}
